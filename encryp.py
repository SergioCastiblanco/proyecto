from cryptography.fernet import Fernet


# solo se necesita una vez para generar la llave
# genera la llave
def generateKey():

    key = Fernet.generate_key()
    with open("secret.key", "wb") as key_file:
        key_file.write(key)


# cargar la llave
def loadKey():
    return open("secret.key", "rb").read()


# encripta
def encryptPass(password):
    # cargar la llave
    key = loadKey()
    # cambia la base
    encodedPass = password.encode()
    # utiliza la libreria
    f = Fernet(key)
    # encripta
    encryptedPass = f.encrypt(encodedPass)

    # devuelve un string
    return encryptedPass.decode('UTF-8')


# desencripta
def decryptPass(cryptedPass):
    # carga la llave
    key = loadKey()
    # llama la libreria
    f = Fernet(key)
    # desencripta
    decryptedPass = f.decrypt(cryptedPass.encode('UTF-8'))
    # devuelve la clave decodificada
    return decryptedPass.decode('UTF-8')