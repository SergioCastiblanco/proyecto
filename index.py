import json
import uuid
from interfaz import menu, menu_usuario, menu_items, menu_tags, datos_validacion, datos_para_registro, datos_crear_item
from funciones import agregar_tag_item, ver_tag, registro, ingresar, crear_item, encriptar_password, lista_tags, crear_item, lista_items, eliminar_tag, editar_item, crear_tag, eliminar_item, ver_item, editar_tag
from encryp import encryptPass, decryptPass


def usuario_in_items(id_usuario):

    print("lista de items id-titulo-url-comentario: \n")
    # se debe mostrar la lista de items
    lista_items(id_usuario)
    print(" -------------------- ")
    # trae la opcion que digito el usuario en menu_items
    m_items = menu_items()

    # verifica la opcion que digito el usuario en menu_items

    # Agrega un item
    if m_items == 1:
        print("\n -------------------- \n")
        # mostrar_lista_items()
        print("lista de items id-titulo-url-comentario: \n")
        lista_items(id_usuario)
        print(" -------------------- ")
        # trae los datos que digito el usuario
        titulo_item, username_item, password, url_item, comentario_item = datos_crear_item()
        # encripta la contraseña
        password_item = encryptPass(password)
        # utilizando la libreria uuid se obtiene el id del item
        id_item = str(uuid.uuid4())
        # llama la funcion para crear el item
        crear_item(id_usuario, titulo_item, username_item,
                   password_item, url_item, comentario_item, id_item)
        # se garega el tag utilizando la funcion agregar_tags y pasandole los parametros id_usuario y id_item
        agregar_tag_item(id_usuario, id_item)
        print(" -------------------- ")
        print("Item creado")
        print("\n -------------------- \n")
        usuario_in_items(id_usuario)

    # edita un item
    elif m_items == 2:
        # se pide el id del item al usuario
        IDitem = input("Por favor digite el ID del item: ")
        # se pasa a la función editar_item con los parametros de entrada
        verifica = editar_item(IDitem, id_usuario)
        # se valida el resultado de la funcion
        if verifica == True:
            print("Editado")
            print("\n -------------------- \n")
            usuario_in_items(id_usuario)
        else:
            print("no se encontro el ID, verifique y vuelva a intentar")
            print("\n -------------------- \n")
            usuario_in_items(id_usuario)

    # elimina un item
    elif m_items == 3:
        # se pide el id del item al usuario
        IDitem = input("Por favor digite el ID del item: ")
        # se pasa a la función eliminar_item con los parametros de entrada
        verifica = eliminar_item(IDitem, id_usuario)
        # se valida el resultado de la funcion
        if verifica == True:
            print("Eliminado")
            print("\n -------------------- \n")
            usuario_in_items(id_usuario)
        else:
            print("no se encontro el ID, verifique y vuelva a intentar")
            print("\n -------------------- \n")
            usuario_in_items(id_usuario)

    # ve un item
    elif m_items == 4:
        # se pide el id del item al usuario
        IDitem = input("Por favor digite el ID del item: ")
        # se asignan las constantes a los valores que devuelve ver_item
        id_item, titulo, username, password, url, comentario, tags, verifica = ver_item(IDitem, id_usuario)
        # se valida el resultado de la funcion
        if verifica == True:
            # se muestran los datos
            print(" -------------------- ")
            print("Id del item: ", id_item)
            print("Titulo del item: ", titulo)
            print("Username del item: ", username)
            print("Password del item: ", password)
            print("URL del item: ", url)
            print("Comentarios del item: ", comentario)
            print("Tags del item: ", tags)
            print("\n -------------------- \n")
            usuario_in_items(id_usuario)

        else:
            print("no se encontro el ID, verifique y vuelva a intentar")
            print("\n -------------------- \n")
            usuario_in_items(id_usuario)

    # volver
    elif m_items == 5:
        print("eligio volver")
        print("\n -------------------- \n")
        usuario(id_usuario)
    else:
        print("Opcion no valida")
        print("\n -------------------- \n")
        usuario_in_items(id_usuario)


def usuario_in_tags(id_usuario):
    # se debe ver la lista de tags
    lista_tags(id_usuario)
    print(" -------------------- ")
    # trae la opcion que digito el usuario en menu_items
    m_tags = menu_tags()
    # verifica la opcion que digito el usuario en menu_items

    # agrega un tag
    if m_tags == 1:
        nuevo_tag = input("Por favor ingrese el tag: ")
        if nuevo_tag != "":
            verifica = crear_tag(nuevo_tag, id_usuario)
            if verifica == True:
                print("El tag se ha creado")
                print("\n -------------------- \n")
                usuario_in_tags(id_usuario)
            else:
                print("El tag ya existe")
                print("\n -------------------- \n")
                usuario_in_tags(id_usuario)
        else:
            print("\n Debe digitar el tag")
            print("\n -------------------- \n")
            usuario_in_tags(id_usuario)

    # edita un tag
    elif m_tags == 2:
        tag = input("Por favor digite el tag: ")
        edit_tag = input("Digite la correccion del tag: ")
        verifica = editar_tag(tag, edit_tag, id_usuario)
        if verifica == True:
            print("El tag ha sido editado")
            print("\n -------------------- \n")
            usuario_in_tags(id_usuario)
        else:
            print("Error, el tag no existe")
            print("\n -------------------- \n")
            usuario_in_tags(id_usuario)

    # elimina un tag
    elif m_tags == 3:
        tag = input("Por favor digite el tag: ")
        verifica = eliminar_tag(tag, id_usuario)
        if verifica == True:
            print("El tag ha sido eliminado")
            print("\n -------------------- \n")
            usuario_in_tags(id_usuario)
        else:
            print("Error, el tag no existe")
            print("\n -------------------- \n")
            usuario_in_tags(id_usuario)

    # ve un tag
    elif m_tags == 4:
        tag = input("Por favor digite el tag: ")
        ver_tag(id_usuario, tag)

    elif m_tags == 5:
        print("eligio volver")
        print("\n -------------------- \n")
        usuario(id_usuario)
    else:
        print("Opcion no valida")
        print("\n -------------------- \n")
        usuario_in_tags(id_usuario)


def usuario(id_usuario):
    # trae la opcion que digito el usuario
    m_usuario = menu_usuario()
    if m_usuario == 1:
        print("\n -------------------- \n")
        usuario_in_items(id_usuario)

    elif m_usuario == 2:
        print("\n -------------------- \n")
        usuario_in_tags(id_usuario)

    elif m_usuario == 3:
        print("\n -------------------- \n")
        print("Nos vemos pronto")
        print("\n -------------------- \n")
        anonimo()
    else:
        print("Opcion no valida")
        print("\n -------------------- \n")
        usuario(id_usuario)


def anonimo():

    # Pide la opción al anonimo
    opcion = menu()

    # verifica la opción que eligio el anonimo
    if opcion == 1:
        print("\n -------------------- \n")
        username, password = datos_validacion()
        validacion, id_usuario = ingresar(username, password)
        # validar credenciales
        if validacion == True:
            usuario(id_usuario)
        else:
            print("Datos erroneos")
            print("\n -------------------- \n")
            anonimo()

    elif opcion == 2:
        print("Eligio Crear una cuenta")
        username, name, password = datos_para_registro()
        # validar que todos los datos tengan información
        if username == "" or password == "" or name == "":
            print("\n  ¡Debe digitar todos los datos!")
            print("\n -------------------- \n")
            anonimo()
        else:
            # se registran los datos
            registro(username, name, password)
            anonimo()

    elif opcion == 3:
        print("\n -------------------- \n")
        print("Gracias por utilizar el programa")
    else:
        print("Opcion no valida")
        print("\n -------------------- \n")
        anonimo()


anonimo()
