import json
import uuid
from encryp import encryptPass, decryptPass
from interfaz import datos_crear_item


def registro(username, name, password):
    print("\n -------------------- \n")
    # creamos el diccionario
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    id1 = str(uuid.uuid4())
    datos["usuarios"].append({

        "id": id1,
        "name": name,
        "username": username,
        "password": password,
        "items": [],
        "tags": []
    })

    # abrimos el archivo .json en modo escritura
    archivo = open('users.json', 'w')
    # escribir el diccionario en el archivo
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    # cerramos el archivo
    archivo.close()


def ingresar(username, password):
    print("\n -------------------- \n")

    # abrimos el archivo .json
    archivo = open('users.json', 'r')
    # lee archivo y obtiene el diccionario
    datos = json.load(archivo)
    # imprime los datos
    archivo.close()

    validar= False
    for usuario in datos["usuarios"]:
        if usuario["username"] == username and usuario["password"] == password:
            usuario_id=usuario["id"]
            validar= True

    return validar, usuario_id



def crear_item(id_usuario, titulo_item, username_item, password_item, url_item, comentario_item, id_item):

    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    for usuario in datos["usuarios"]:
        if id_usuario == usuario["id"]:
            usuario["items"].append({
                "id": id_item,
                "Titulo": titulo_item,
                "Username": username_item,
                "password": password_item,
                "url": url_item,
                "comentario": comentario_item,
                "Tags": []
            })

    archivo = open('users.json', 'w')
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    archivo.close()


def validar_tag(id_usuario, tag_usuario):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    validacion = False
    for usuario in datos["usuarios"]:
        if id_usuario == id_usuario:
            for tag in usuario["tags"]:
                if tag_usuario == tag:
                    validacion = True
    return validacion


def agregar_tag_item(id_usuario, id_item):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()
    tag_u = input("Por favor digite el tag: ")
    verifica = False
    validacion=validar_tag(id_usuario, tag_u)
    if validacion == True:
        for usuario in datos["usuarios"]:
            if id_usuario == usuario["id"]:
                for item in usuario["items"]:
                    if item["id"] == id_item:
                            item["Tags"].append(tag_u)
                    else:
                        item["Tags"] = item["Tags"]
                        verifica = True
    archivo = open('users.json', 'w')
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    archivo.close()
    return verifica



def encriptar_password(password_item):
    password_encript = encryptPass(password_item)
    return password_encript


def lista_items(id_usuario):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    for usuario in datos["usuarios"]:
        if usuario["id"] == id_usuario:
            for item in usuario["items"]:
                    print(item["id"], " - ", item["Titulo"], " - ",
                        item["url"], " - ", item["comentario"])


def editar_item(Id_item, id_usuario):

    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()
    tag_u = input("Por favor digite el tag: ")
    tag_p = ""
    verifica = False
    for usuario in datos["usuarios"]:
        if usuario["id"] == id_usuario:
            for tag in usuario["tags"]:
                if tag == tag_u:
                    tag_p = tag_u
                else:
                    tag_p = ""
            for item in usuario["items"]:
                item["Tags"] = []
                if Id_item == item["id"]:
                    nuevo_titulo, nuevo_username, nuevo_password, nuevo_url, nuevo_comentario = datos_crear_item()
                    item["Titulo"] = nuevo_titulo
                    item["Username"] = nuevo_username
                    n_password=encryptPass(nuevo_password)
                    item["password"] = n_password
                    item["url"] = nuevo_url
                    item["comentario"] = nuevo_comentario
                    if tag_p != "":
                        item["Tags"].append(tag_p)

                    verifica = True

    archivo = open('users.json', 'w')
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    archivo.close()
    return verifica


def eliminar_item(Id_item, id_usuario):

    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    verifica = False
    for usuario in datos["usuarios"]:
        if id_usuario == usuario["id"]:
            for item in usuario["items"]:
                if Id_item == item["id"]:

                    lista_items = usuario["items"]

                    indice = lista_items.index(item)

                    seguro = int(
                        input("¿Desea borrar el item? \n 1. Si  \n 2. No \n : "))
                    if seguro == 1:
                        del(lista_items[indice])
                    elif seguro == 2:
                        print("Proceso abortado")
                    else:
                        print("opción no valida")
                    verifica = True
    archivo = open('users.json', 'w')
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    archivo.close()
    return verifica


def ver_item(Id_item, id_usuario):

    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    for usuario in datos["usuarios"]:
        if id_usuario == usuario["id"]:
            for item in usuario["items"]:
                if Id_item == item["id"]:
                    id3 = item["id"]
                    titulo = item["Titulo"]
                    username = item["Username"]
                    password_e = item["password"]
                    password = decryptPass(password_e)
                    url = item["url"]
                    comentario = item["comentario"]
                    tags = item["Tags"]
                    verifica = True
                    return id3, titulo, username, password, url, comentario, tags, verifica
                else:
                    verifica = False
    return "", "", "", "", "", "", "", verifica


def crear_tag(nuevo_tag, id_usuario):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()
    validar = False
    for usuario in datos["usuarios"]:
        if usuario["id"] == id_usuario:
            if nuevo_tag in usuario["tags"]:
                validar = validar
            else:
                usuario["tags"].append(nuevo_tag)
                validar = True

    archivo = open('users.json', 'w')
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    archivo.close()
    return validar


def editar_tag(tag, edit_tag, id_usuario):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    validar = False
    for usuario in datos["usuarios"]:
        if usuario["id"] == id_usuario:
            if tag in usuario["tags"]:
                lista_tags = usuario["tags"]
                indice = lista_tags.index(tag)
                lista_tags[indice] = edit_tag
                validar = True
            else:
                validar = validar
    archivo = open('users.json', 'w')
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    archivo.close()
    return validar


def eliminar_tag(tag, id_usuario):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    validar = False
    for usuario in datos["usuarios"]:
        if usuario["id"] == id_usuario:
            if tag in usuario["tags"]:
                lista_tags = usuario["tags"]
                seguro = int(
                    input("¿Desea borrar el tag? \n 1. Si  \n 2. No \n : "))
                if seguro == 1:
                    lista_tags.remove(tag)
                elif seguro == 2:
                    print("Proceso abortado")
                else:
                    print("opción no valida")
                validar = True
    archivo = open('users.json', 'w')
    json.dump(datos, archivo, indent=4, separators=(',', ': '))
    archivo.close()
    return validar


def lista_tags(id_usuario):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()
    for usuario in datos["usuarios"]:
        if usuario["id"] == id_usuario:
            lista_tags = usuario["tags"]
    print(lista_tags)


def ver_tag(id_usuario, tag_usuario):
    archivo = open('users.json', 'r')
    datos = json.load(archivo)
    archivo.close()

    
    for usuario in datos["usuario"]:
        if usuario["id"] == id_usuario:
            for tag in usuario["tags"]:
                if tag == tag_usuario:
                    existe = True
                else:
                    print("Error, el tag no existe")
                    print("\n -------------------- \n")
            if existe == True:
                for item in usuario["items"]:
                    for tag in item["Tags"]:
                        if tag == tag_usuario:
                            print(item["id"])
                            print(item["Titulo"])
                            print(item["Username"])
                            print(item["url"])
