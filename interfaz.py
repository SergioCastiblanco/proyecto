

def menu():
    '''
    Muestra el menu de opciones al anonimo
    '''
    print("1. Ingresar")
    print("2. Crear una cuenta")
    print("3. Salir")
    opcion = int(input("Por favor elija una de la opciones: "))
    return opcion


def datos_para_registro():
    '''
    le pide al usuario los datos para el registro
    '''
    username = input("Digite un username: ")
    name = input("Digite su nombre: ")
    password = input("Digite una contraseña: ")

    return username, name, password


def datos_validacion():
    '''
    pide las credenciales al anonimo
    '''
    username = input("Por favor ingrese su username: ")
    password = input("Por favor ingrese su contraseña: ")
    return username, password


def menu_usuario():
    '''
    Muestra el menu de opciones al usuario
    '''
    print("1. Ver Items")
    print("2. Ver Tags")
    print("3. Cerrar sesion")
    opcion1 = int(input("Por favor elija una de la opciones: "))
    return opcion1


def menu_items():
    '''
    Muestra el menu de opciones al usuario una vez se encuentra en ítems
    '''
    print("1. Agregar Item")
    print("2. Editar Item")
    print("3. Eliminar Item")
    print("4. Ver Item")
    print("5. Volver")
    opcion2 = int(input("Por favor elija una de la opciones: "))
    return opcion2


def menu_tags():
    '''
    Muestra el menu de opciones al usuario una vez se encuentra en tags
    '''
    print("1. Agregar Tag")
    print("2. Editar Tag")
    print("3. Eliminar Tag")
    print("4. Ver Tag")
    print("5. Volver")
    opcion3 = int(input("Por favor elija una de la opciones: "))
    return opcion3


def datos_crear_item():
    '''
    le pide al usuario los datos para crear el item
    '''
    titulo = input("Por favor ingrese el tiulo del item: ")
    username = input("Por favor ingrese el username del item: ")
    password = input("Por favor ingrese el password del item: ")
    url = input("Por favor ingrese el url del item: ")
    comentario = input("Por favor ingrese un comentario del item: ")

    if titulo == "" or username == "" or password == "" or url == "":
        print("\n -------------------- \n")
        print(" ¡Debe ingresar todos los datos!")
        print("\n -------------------- \n")
        datos_crear_item()
    else:
        return titulo, username, password, url, comentario

